package automationpanda.example.framework.runners;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber", "junit:target/cucumber.xml"},
        features = "src\\test\\resources\\example.feature",
        glue = {"automationpanda\\example\\framework\\stepsdefinitions"}
)
public class CucumberTest {
}
