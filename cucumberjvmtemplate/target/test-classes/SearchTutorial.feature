Feature: Search tutorials in JavaPointers

  Scenario: A user search for a tutorial, the user related content results should be displayed in UI
    Given User is on the main page of www.javapointers.com
    When User search for Cucumber Java Tutorial
    Then search page should be displayed with related contents