Feature: Google Homepage
  This feature verifies the functionality of Google Homepage are displayed

  Scenario: Check that main elements on Google Homepage are displayed

    Given I launch Chrome browser
    When I open Google Homepage
    Then I verify that the page displays search text box
    And the page displays Google search button
    And the page displays I'm feeling Lucky button