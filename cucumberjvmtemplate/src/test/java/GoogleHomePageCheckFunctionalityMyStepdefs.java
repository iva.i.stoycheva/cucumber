import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GoogleHomePageCheckFunctionalityMyStepdefs {
    @Given("I launch Chrome browser")
    public void iLaunchChromeBrowser() {
    }

    @When("I open Google Homepage")
    public void iOpenGoogleHomepage() {
    }

    @Then("I verify that the page displays search text box")
    public void iVerifyThatThePageDisplaysSearchTextBox() {
    }

    @And("the page displays Google search button")
    public void thePageDisplaysGoogleSearchButton() {
    }

    @And("the page displays I'm feeling Lucky button")
    public void thePageDisplaysIMFeelingLuckyButton() {
    }
}
