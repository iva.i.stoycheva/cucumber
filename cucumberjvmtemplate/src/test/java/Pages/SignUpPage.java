package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SignUpPage {

    WebDriver driver;

    public  SignUpPage(WebDriver driver){
        this.driver = driver;
    }

    public void she_provides_the_email_as(String email) throws Throwable {
        driver.findElement(By.xpath("//*[@id=\"loginfrm\"]/div[3]/div[1]/label/span")).sendKeys("email");
    }

    public void she_provides_the_password_as(String password) throws Throwable{
        driver.findElement(By.xpath("//*[@id=\"loginfrm\"]/div[3]/div[2]/label/span")).sendKeys(password);
    }
}
