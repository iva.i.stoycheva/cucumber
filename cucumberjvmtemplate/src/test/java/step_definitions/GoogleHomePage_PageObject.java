package step_definitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleHomePage_PageObject {
    public static void main(String[] args) {

        // instantiate web driver

        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.bg/");
        driver.manage().window().maximize();

        //verify search box is displayed on the page
        if (driver.findElement(By.xpath("//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input")).isDisplayed()){
            System.out.println("Google search button is not displayed");
        }

        //verify that googleSearch button is displayed
        if(!driver.findElement(By.name("q")).isDisplayed()){
            System.out.println("I'm feeling lucky button not displayed");
        }
    }
}
