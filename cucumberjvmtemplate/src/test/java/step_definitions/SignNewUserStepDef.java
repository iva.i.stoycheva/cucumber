package step_definitions;

import Pages.HomePage;
import Pages.LandingPage;
import Pages.SignUpPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SignNewUserStepDef {

    WebDriver driver = new ChromeDriver();


    @Given("^the user is on landing page$")

    public void setup() throws  Throwable {
        driver.get("https://www.phptravels.net/");
        driver.manage().window().maximize();
    }

    @When("^she chooses to sign up$")
    public void sheChoosesToSignUp() throws Throwable {
        new LandingPage(driver).she_chooses_to_sign_up();
    }


    @And("she provides the email as ([^\\\"]*)$")
    public void she_provides_the_email_as(String email) throws Throwable {
        new SignUpPage(driver).she_provides_the_email_as(email);
    }

    @And("^she provides the password as ([^\\\"]*)$")
    public void she_provides_the_password_as(String password) throws Throwable {
        new SignUpPage(driver).she_provides_the_password_as(password);
    }
/*
    @And("^she signs-up$")
    public void she_signs_up() throws Throwable {
        new SignUpPage(driver).she_signs_up();
    }*/

    @And("she provides the email as {string}")
    public void sheProvidesTheEmailAs(String arg0) {
    }

    @And("she provides the password as {string}")
    public void sheProvidesThePasswordAs(String arg0) {
    }

    @Then("she should be logged in to the application")
    public void sheShouldBeLoggedInToTheApplication() {
    }
}
