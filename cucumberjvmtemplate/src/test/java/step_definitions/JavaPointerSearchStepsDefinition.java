package step_definitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class JavaPointerSearchStepsDefinition {
    @Given("User is on the main page of www.javapointers.com")
    public void userIsOnTheMainPageOfWwwJavapointersCom() {
    }

    @When("User search for Cucumber Java Tutorial")
    public void userSearchForCucumberJavaTutorial() {
    }

    @Then("search page should be displayed with related contents")
    public void searchPageShouldBeDisplayedWithRelatedContents() {
    }
}
