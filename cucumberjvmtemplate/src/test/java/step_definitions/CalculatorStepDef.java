package step_definitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CalculatorStepDef {
    @Given("the calculator has an addition program")
    public void theCalculatorHasAnAdditionProgram() {
    }

    @When("i choose to add {int} numbers")
    public void iChooseToAddNumbers(int arg0) {
    }

    @And("i add {int} and {int}")
    public void iAddAnd(int arg0, int arg1) {
    }

    @Then("the result is {int}")
    public void theResultIs(int arg0) {
    }
}
