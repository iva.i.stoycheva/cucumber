
Feature:Feature:
  As a user
  I want to be able to add new clients in the system
  So that i can add accounting data for that client

  Scenario: Sign up a new user

Given the user is on landing page
When she chooses to sign up
And she provides the email as "validemail@aq.com"
And she provides the password as "password"
Then she should be logged in to the application